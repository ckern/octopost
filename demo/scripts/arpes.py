# -*- coding: utf-8 -*-
"""Example script showing how to calculate, plot and export ARPES maps from
a TDDFT-PES Octopus simulation.
"""

import octopost
from pathlib import Path
import matplotlib.pyplot as plt

# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/C60')

# Matplotlib customizability
plt.rcParams['image.cmap'] = 'twilight'

# Binding energy in eV
E = -6.157
# Omega (if parser.log is present can be read automatically -> see 'field'
# input option)
omega= 35

# This function needs to be pointed to the specific PES file desired to be read
# Setting E_num to 1 will return a single map at E_range[0]
maps, E_axis, k_axis = op.get_gasphase_arpes(E_num=1, E_range=[E, E], dE_b=.1,
                                             omega=omega)
# Minor convenience function to plot a single map
fig, ax = op.get_arpes_plot(maps[0, :, :], k_axis)
plt.title(f'ARPES of C60 at {E} eV')


# Writes a hdf5 file (readable by kMap.py for example) with 10 (default) maps
# at binding energies from -10 to 0 eV and a (kx, ky) resolution of (250, 250)
# (Path is relative to the root folder specified at line 13)
# op.get_gasphase_arpes(out=True, out_file='../../output/arpes.hdf5',
#                       omega=omega, resolution=250)

# fig.savefig(dir_path / '../output/arpes.png', dpi=150)
# plt.show()
