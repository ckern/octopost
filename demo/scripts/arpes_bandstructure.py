# -*- coding: utf-8 -*-
"""Example script showing how to plot the bandstructure from an ARPES
calculation and how to overlay it with the regularly calculated bandstructure.
"""

import octopost
from units import energy

import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt

# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

# Pass the photon energy if no parser.log is available
omega = 35
# List of k path steps (as specified in 'inp')
# Here we go 15 points to the first notable point (K), then another 368 to the
# next point (K) and the path finishes after a total of 15+368+15=398 steps
# Note: This means there are 399 points in total
k_points = [15, 368, 15]
# Label the we specified above
labels = ['K', 'K', '']

# Get the bandstructure from a ARPES calculation
path, _, (fig, ax) = op.get_arpes_bandstructure(
    omega=omega, k_points=k_points, labels=labels)

# Overlay the regularly calculated bandstructure
# (only bands 6 to 12 and they start from the the middle point in this case
bands = list(range(6, 13))
_, bands_data = op.get_bandstructure(bands=bands, plot=False,
                                     energy_units='eV')
x = np.linspace(path[int(len(path)/2)], path[-1], len(bands_data[:, 0]),
                endpoint=True)
for i, _ in enumerate(bands):
    ax.plot(x, bands_data[:, i], color='k')

# fig.savefig(dir_path / '../output/arpes_bandstructure.png', dpi=150)
# plt.show()
