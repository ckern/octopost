# -*- coding: utf-8 -*-
"""Example script showing how to extract and plot an atom projected density of
states.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# A new Octopost object is required for reading and analyzing the data
# It needs the absolute path to the root directory (where 'inp' is) of the
# calulation output.
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/1P_Cu110')

# DOS
_, (fig, ax) = op.get_total_dos(plot=True, energy_units='eV')

# Substrate
sub_atoms = list(range(1, 28))
dos = op.get_atomic_dos(atom=sub_atoms[0], energy_units='eV')
for atom_number in sub_atoms[1:]:
    dos[:, 1] += op.get_atomic_dos(atom=atom_number, energy_units='eV')[:, 1]

# Arbitrary scaling factors for visual purpose
scaling = 2 / len(sub_atoms)
ax.plot(dos[:, 0], scaling * dos[:, 1], label='Sub')

# Molecule
mol_atoms = list(range(28, 40))
dos = op.get_atomic_dos(atom=mol_atoms[0], energy_units='eV')
for atom_number in mol_atoms[1:]:
    dos[:, 1] += op.get_atomic_dos(atom=atom_number, energy_units='eV')[:, 1]

# Arbitrary scaling factors for visual purpose
scaling = 5 / len(mol_atoms)
ax.plot(dos[:, 0], scaling * dos[:, 1], label='Mol')

ax.legend()
ax.set_title('Atom Resolved DOS')

# fig.savefig(dir_path / '../output/atom_dos.png', dpi=150)
# plt.show()
