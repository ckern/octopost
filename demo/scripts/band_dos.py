# -*- coding: utf-8 -*-
"""Example script showing how to extract and plot a band projected density of
states.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# A new Octopost object is required for reading and analyzing the data
# It needs the absolute path to the root directory (where 'inp' is) of the
# calulation output.
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/C60')

n_min, n_max = 107, 120
# n_min, n_max = 1, -1 # all bands
# Get the total density of states as well as plot
# Use different flags for basic customizability
_, (fig, ax) = op.get_total_dos(plot=True, energy_units='eV')
# Add the band resolved dos for the bands 4-10
_, (fig, ax) = op.get_band_dos(
    n_min=n_min, n_max=n_max, plot=True, energy_units='eV', axes=(fig, ax),
    plot_fermi=False)

# Plot the total contribution of all selected bands
plt.rcParams.update({'lines.linestyle': '--'})
dos, (fig, ax) = op.get_band_dos(
    n_min=n_min, n_max=n_max, plot=True, combined=True,
    energy_units='eV', axes=(fig, ax), plot_fermi=False)

ax.set_xlim([-5.5, 7.5])

# fig.savefig(dir_path / '../output/band_dos.png', dpi=150)
# plt.show()
