# -*- coding: utf-8 -*-
"""Example script showing how to extract, plot and customize the bandstructure
from an Octopus calulation.
"""

import octopost
from pathlib import Path
import matplotlib.pyplot as plt

# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

# Calling this function returns the bandstructure from the 'bandstructure' file
# if available. An optional 'plot' flag (default = False) will
# additionally return matplotlib handles for a basic plot
*_, (fig, ax) = op.get_bandstructure(bands=list(range(6, 13)), plot=True,
                                        energy_units='eV')

# Use the the handles to customize the plot
ax.set_title('Bandstructure of MoS2')

# fig.savefig(dir_path / '../output/bandstructure.png', dpi=150)
# plt.show()
