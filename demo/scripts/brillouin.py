# -*- coding: utf-8 -*-
"""Example script showing how to plot the first Brillouin zone and the reci-
procal unit vectors.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# A new Octopost object is required for reading and analyzing the data
# It needs the absolute path to the root directory (where 'inp' is) of the
# calulation output.
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

fig, ax = op.plot_reciprocal()

# fig.savefig(dir_path / '../output/brillouin.png', dpi=150)
# plt.show()
