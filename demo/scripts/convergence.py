# -*- coding: utf-8 -*-
"""Example script showing how to extract and plot the convergence of an
Octopus calculation.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

# Plot the convergence over the individual SCF cycles for energy and density
data, (fig, (ax_left, ax_right)) = op.get_convergence(plot=True)
# fig.savefig(dir_path / '../output/convergence.png', dpi=150)

# Same as above but only the energy
data, (fig, (ax_left)) = op.get_convergence(plot=True, values=('energy'))

# plt.show()
