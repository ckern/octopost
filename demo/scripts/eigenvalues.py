# -*- coding: utf-8 -*-
"""Example script showing how to read the fermi energy and eigenvalues of an
Octopus calculation.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

# Calling this function will return all eigenvalues available for the 6th
# k-point in units of eV
eigenvalues = op.get_eigenvalues(n_max=None, k_point=6, energy_units='eV')
# print(eigenvalues)

# Using this function to set the Ocotopost instance to a new location
op.set_root(dir_path / '../data/1P_Cu110')

# Calling this function will return the fermi energy in units of Hartree
fermi = op.get_fermi(energy_units='hartree')
# print(fermi)