# -*- coding: utf-8 -*-
"""Example script showing how to extract the lattice and reciprocal lattice
from a periodic Octopus calculation.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/MoS2')

# Get real space unit cell vectors
cell = op.get_cell()
# print(cell)

# Get reciprical unit cell vectors
cell = op.get_reciprocal_cell()
# print(cell)
