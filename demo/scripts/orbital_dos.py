# -*- coding: utf-8 -*-
"""Example script showing how to extract and plot an orbital projected density
of states.
"""

import octopost
import matplotlib.pyplot as plt
from pathlib import Path


# A new Octopost object is required for reading and analyzing the data
# It needs the absolute path to the root directory (where 'inp' is) of the
# calulation output.
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/1P_Cu110')

# Get the total density of states as well as plot
# Use different flags for basic customizability
_, (fig, ax) = op.get_total_dos(plot=True, energy_units='eV')
# Add the orbital dos for C2p and C2s
_, (fig, ax) = op.get_orbital_dos(
    'Cu4s', plot=True, energy_units='eV', axes=(fig, ax), plot_fermi=False)
dos, (fig, ax) = op.get_orbital_dos(
    'C2s', plot=True, energy_units='eV', axes=(fig, ax), plot_fermi=False)
dos, (fig, ax) = op.get_orbital_dos(
    'C2p', plot=True, energy_units='eV', axes=(fig, ax), plot_fermi=False)
dos, (fig, ax) = op.get_orbital_dos(
    'H1s', plot=True, energy_units='eV', axes=(fig, ax), plot_fermi=False)

ax.set_title('Orbital Resolved DOS')

# Note: Orbitals in Octopus are not orthonormalized -> sum does not equal
# total dos
# fig.savefig(dir_path / '../output/orbital_dos.png', dpi=150)
# plt.show()

