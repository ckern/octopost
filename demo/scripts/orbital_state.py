# -*- coding: utf-8 -*-
"""Example script showing how to extract the homo/lumo state number.
   ATTENTION: This only works for non periodic systems!
"""

import octopost
from pathlib import Path


# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent

op = octopost.Octopost(dir_path / '../data/C60')

# Calling this function returns the corresponding state number. Useful for
# other metods in Octopost.
homo = op.get_orbital_state_number()
# print(homo)
lumo = op.get_orbital_state_number('lumo')
# print(lumo)
lumo_2 = op.get_orbital_state_number('lumo+2')
# print(lumo_2)
homo_1 = op.get_orbital_state_number('homo-1')
# print(homo_1)
