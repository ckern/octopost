# -*- coding: utf-8 -*-
"""Example script showing how to plot an integrated photoemission map.
"""

import octopost
from pathlib import Path
import matplotlib.pyplot as plt

# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/C60')

# Show the photoemission intensity dependent of kinetic energy
pes, E_axis, (fig, ax) = op.get_gasphase_pes('PES_velocity_map.vtk', plot=True)
ax.set_title('Energy-dependent intensity of photoemission')
# fig.savefig(dir_path / '../output/pes_of_energy.png', dpi=150)

# Show the photoemission intensity dependent of the azimuthal angle phi
pes, phi_axis, (fig, ax) = op.get_gasphase_pes(
    'PES_velocity_map.vtk', integrate=('energy', 'phi'), plot=True)
ax.set_title('Theta-dependent intensity of photoemission')
# fig.savefig(dir_path / '../output/pes_of_phi.png', dpi=150)

# Integrate over the entire semisphere, i.e. the total photoemission
total_pes = op.get_gasphase_pes('PES_velocity_map.vtk',
                                integrate=('energy', 'phi', 'theta'))

# print(total_pes)
# plt.show()



