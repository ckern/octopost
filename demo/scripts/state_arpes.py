# -*- coding: utf-8 -*-
"""Example script showing how to calculate and plot K.S. state resolved
ARPES maps.
"""

import octopost
from pathlib import Path
import matplotlib.pyplot as plt

# Instance new 'Octopost' class and pass it the absolute path to the root
# directory of the calculation ouput (where the 'inp' file usually is)
dir_path = Path(__file__).parent
op = octopost.Octopost(dir_path / '../data/C60')

# Matplotlib customizability
plt.rcParams['image.cmap'] = 'twilight'

# This function returns the K.S. state projected arpes maps for the states
# 107 to 121 with a energy integration of 0.5 eV.
states = list(range(107, 121))
maps, E_bs, k_axis = op.get_state_resolved_arpes(states=states, omega=21.2,
                                                 dE_b=0.5)
# Get the state number of the HOMO orbital and find it in states list
state_number = op.get_orbital_state_number('HOMO')
list_number = states.index(state_number)

fig, ax = op.get_arpes_plot(maps[list_number, :, :], k_axis)
plt.title(f'HOMO of C60 projected ARPES map at {E_bs[list_number]:.03f} eV')
# fig.savefig(dir_path / '../output/C60_HOMO.png', dpi=150)

# Get the HOMO directly
# (should be same output as more cumbersome approach before)
maps, E_bs, k_axis = op.get_state_resolved_arpes(states=['HOMO'], omega=21.2,
                                                 dE_b=0.5)
fig, ax = op.get_arpes_plot(maps[0, :, :], k_axis)
plt.title(f'HOMO of C60 projected ARPES map at {E_bs[0]:.03f} eV (direct)')

# Get the HOMO-1 directly
maps, E_bs, k_axis = op.get_state_resolved_arpes(states=['HOMO-1'], omega=21.2,
                                                 dE_b=0.5)
fig, ax = op.get_arpes_plot(maps[0, :, :], k_axis)
plt.title(f'HOMO-1 of C60 projected ARPES map at {E_bs[0]:.03f} eV')
# fig.savefig(dir_path / '../output/PTCDA_HOMO-1.png', dpi=150)

# plt.show()
