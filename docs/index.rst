====================================
Welcome to Octopost's documentation!
====================================

**Octopost** contains a pool of python3 tools for post-processing data obtained with the TDDFT code OCTOPUS[1].

Contents
========

.. toctree::
   :maxdepth: 2

   Introduction <introduction>
   Code Documentation <module>
