====================================
Code Documenation
====================================

ARPES
======
.. automodule:: arpes_model
   :members:

DOS
======
.. automodule:: dos_model
   :members:

Bandstructure
==============
.. automodule:: bandstructure_model
   :members:

Info
=====
.. automodule:: output_read_model
   :members:

Input
======
.. automodule:: input_parser
   :members:

Units
=====
.. automodule:: units
   :members:

Library
=======
.. automodule:: library
   :members:

.. toctree::
